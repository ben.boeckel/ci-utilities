#!/bin/sh

set -e

# Install build requirements.
yum install -y \
    libX11-devel libXext-devel libXrandr-devel \
    bison flex chrpath

# Install EPEL
yum install -y \
    epel-release

# Install toolchains.
yum install -y \
    centos-release-scl
yum install -y \
    devtoolset-10-gcc-c++ \
    devtoolset-10 \
    devtoolset-10-gcc \
    devtoolset-10-gfortran \
    rh-git227-git-core

yum clean all
