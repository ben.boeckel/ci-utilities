cmake_minimum_required(VERSION 3.8)

# Set up the source and build paths.
set(CTEST_SOURCE_DIRECTORY "$ENV{CI_PROJECT_DIR}/finder-tags")
set(CTEST_BINARY_DIRECTORY "$ENV{CI_PROJECT_DIR}/build")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")

ctest_start(Experimental)

# Configure the project.
ctest_configure(
  RETURN_VALUE configure_result)

if (configure_result)
  message(FATAL_ERROR
    "Failed to configure")
endif ()

ctest_build(
  NUMBER_WARNINGS num_warnings
  RETURN_VALUE build_result)

if (build_result)
  message(FATAL_ERROR
    "Failed to build")
endif ()

ctest_test(
  RETURN_VALUE test_result)

if (test_result)
  ctest_submit(PARTS Done)
  message(FATAL_ERROR
    "Failed to test")
endif ()
