cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(ccache-superbuild)

include(ExternalProject)
set(version 4.6.1)

macro(ccache_project_add name)
  ExternalProject_Add(${name}
  GIT_REPOSITORY    https://github.com/ccache/ccache.git
  GIT_TAG           v${version}
  CMAKE_ARGS
    -DCMAKE_BUILD_TYPE=Release
    -DENABLE_TESTING=OFF
    -DHIREDIS_FROM_INTERNET=ON
    -DZSTD_FROM_INTERNET=ON
    -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}/install
    ${ARGN}
)
endmacro()
macro(ccache_project_pack name arch)
  string(TOLOWER ${CMAKE_SYSTEM_NAME} platform)
  if (platform STREQUAL "darwin")
    set(platform macos)
  endif()
  ExternalProject_Add_Step(${name} pack
   COMMAND ${CMAKE_COMMAND} -E copy bin/ccache ccache
   COMMAND ${CMAKE_COMMAND} -E tar czvf ccache-${version}-${platform}-${arch}.tar.gz ccache
   WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/install
   DEPENDEES install
   )
endmacro()

if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux")
  ccache_project_add(ccache-linux
    -DCMAKE_EXE_LINKER_FLAGS=-static-libstdc++\ -static-libgcc
    )
  ccache_project_pack(ccache-linux ${CMAKE_SYSTEM_PROCESSOR})

elseif(CMAKE_HOST_SYSTEM_NAME STREQUAL "Darwin")
  ccache_project_add(ccache-mac-x86_64
    -DCMAKE_OSX_DEPLOYMENT_TARGET="12.4"
    -DCMAKE_OSX_ARCHITECTURES="x86_64"
    -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}/install/x86_64
    )
  ccache_project_add(ccache-mac-arm64
    -DCMAKE_OSX_DEPLOYMENT_TARGET="12.4"
    -DCMAKE_OSX_ARCHITECTURES="arm64"
    -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}/install/arm64
  )
 ExternalProject_Add(ccache-mac-universal
   DOWNLOAD_COMMAND ""
   CONFIGURE_COMMAND ""
   BUILD_COMMAND ""
   INSTALL_COMMAND mkdir -p ${CMAKE_SOURCE_DIR}/install/bin
   COMMAND lipo -create -output "bin/ccache" "x86_64/bin/ccache" "arm64/bin/ccache"
   BINARY_DIR ${CMAKE_SOURCE_DIR}/install
   DEPENDS ccache-mac-x86_64 ccache-mac-arm64
   )
  ccache_project_pack(ccache-mac-universal universal)

elseif(CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
  ccache_project_add(ccache-windows)
  ccache_project_pack(ccache-windows ${CMAKE_SYSTEM_PROCESSOR})

else()
  message(FATAL_ERROR "Unrecognized platform ${CMAKE_HOST_SYSTEM_NAME}")
endif()
