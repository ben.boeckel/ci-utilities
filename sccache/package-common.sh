readonly git_url='https://github.com/mozilla/sccache.git'
readonly git_commit='66bad1e8220f6144e2f171e43eda04cb1826dfa6' # v0.4.0-pre7
readonly version='0.4.0-pre7'

git clone "$git_url" sccache/src
cd sccache/src
git -c advice.detachedHead=false checkout "$git_commit"
